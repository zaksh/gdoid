/* $Id: srtp_ping.c,v 1.2 2002/05/09 19:35:24 bew Exp $ */
/* $Source: /nfs/cscbz/gdoi/gdoicvs/gdoi/client_test/srtp_ping.c,v $ */

/* 
 * The license applies to all software incorporated in the "Cisco GDOI reference
 * implementation" except for those portions incorporating third party software 
 * specifically identified as being licensed under separate license. 
 *  
 *  
 * The Cisco Systems Public Software License, Version 1.0 
 * Copyright (c) 2001-2002 Cisco Systems, Inc. All rights reserved.
 * Subject to the following terms and conditions, Cisco Systems, Inc., 
 * hereby grants you a worldwide, royalty-free, nonexclusive, license, 
 * subject to third party intellectual property claims, to create 
 * derivative works of the Licensed Code and to reproduce, display, 
 * perform, sublicense, distribute such Licensed Code and derivative works. 
 * All rights not expressly granted herein are reserved. 
 * 1.      Redistributions of source code must retain the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer.
 * 2.      Redistributions in binary form must reproduce the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 3.      The names Cisco and "Cisco GDOI reference implementation" must not 
 * be used to endorse or promote products derived from this software without 
 * prior written permission. For written permission, please contact 
 * opensource@cisco.com.
 * 4.      Products derived from this software may not be called 
 * "Cisco" or "Cisco GDOI reference implementation", nor may "Cisco" or 
 * "Cisco GDOI reference implementation" appear in 
 * their name, without prior written permission of Cisco Systems, Inc.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE, TITLE AND NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT 
 * SHALL CISCO SYSTEMS, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF 
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. THIS LIMITATION OF LIABILITY SHALL NOT APPLY TO 
 * LIABILITY FOR DEATH OR PERSONAL INJURY RESULTING FROM SUCH 
 * PARTY'S NEGLIGENCE TO THE EXTENT APPLICABLE LAW PROHIBITS SUCH 
 * LIMITATION. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR 
 * LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THAT 
 * EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU. FURTHER, YOU 
 * AGREE THAT IN NO EVENT WILL CISCO'S LIABILITY UNDER OR RELATED TO 
 * THIS AGREEMENT EXCEED AMOUNT FIVE THOUSAND DOLLARS (US) 
 * (US$5,000). 
 *  
 * ====================================================================
 * This software consists of voluntary contributions made by Cisco Systems, 
 * Inc. and many individuals on behalf of Cisco Systems, Inc. For more 
 * information on Cisco Systems, Inc., please see <http://www.cisco.com/>.
 *
 * This product includes software developed by Ericsson Radio Systems.
 */ 

/*
 * srtp_ping -- This program demonstrates how a Secure RTP application 
 *              contacts a GDOI client daemon for SRTP keys and policy.
 */

#include <sys/types.h>
#include <sys/socket.h>
#include <sys/uio.h>
#include <sys/un.h>
#include <netinet/in.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <unistd.h>

#define SRTP_CLIENT_PIPE "/tmp/srtp_to_gdoi"
#define GDOI_CLIENT_PIPE "/tmp/gdoi_to_srtp"

#define MAX_MSG_SIZE 500 /* Guess */
#define MAX_PRINT_BUF_LEN 80

#define ATTR_HDR_SZ 4

/*
 * HEADER TYPE
 */
struct cmd_header {
  short version;
  short command;
#define COMMAND_ADD 3 
#define COMMAND_GET 5 
  int peer_errno;
  int sequence;
  int pid;
};

/*
 * COMMAND_GET Attributes
 */
#define SSRC						1
#define DESTINATION_ADDRESS			2
#define DESTINATION_RTP_PORT		3
#define DESTINATION_RTCP_PORT		4
#define ROLLOVER_COUNTER			5
#define CIPHER						6
#define CIPHER_MODE					7
#define CIPHER_KEY_LENGTH			8
#define SALT_KEY_LENGTH				9
#define AUTHENTICATION_ALGORITHM 	10
#define REPLAY_WINDOW_SIZE			11
#define SRTCP 						12

#define SPI							128
#define GROUPID_KEYID				129
#define RETURN_PIPE   				130
#define CIPHER_KEY   				131

/*
 * STRUCTURES
 *
 * Generic Header
   0                   1                   2                   3
   0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1 2 3 4 5 6 7 8 9 0 1
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |            Version            |            Command            |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                             Errno                             |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                            Sequence                           |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
   |                              PID                              |
   +-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+
 */

/*
 * For testing
 */
union {
  int g;
  char g_char[4];
} default_group;

void err (char *tag)
{
    printf("error: %s", tag);
    if (errno) {
	printf(", errno=%s", strerror(errno));
    }
    printf("\n");
    exit(1);
}

char *grow_buf (char *old_buf, int *old_buf_sz, char *build_buf, 
				int build_buf_sz)
{
  char *new_buf;
  int new_buf_sz = *old_buf_sz + build_buf_sz;

  new_buf = realloc(old_buf, new_buf_sz);
  if (!new_buf)
    {
	  err("realloc failed");
	}
  memcpy((new_buf+*old_buf_sz), build_buf, build_buf_sz);
  *old_buf_sz = new_buf_sz;

  return new_buf;
}

void
encode_16 (char *cp, short x)
{
  *cp++ = x >> 8;
  *cp = x & 0xff;
}

u_int16_t
decode_16 (u_int8_t *cp)
{
  return cp[0] << 8 | cp[1];
}

u_int32_t
decode_32 (u_int8_t *cp)
{
  return cp[0] << 24 | cp[1] << 16 | cp[2] << 8 | cp[3];
}

char *
attribute_add_var (char *buf, int *buf_sz, short type, char *value, short len)
{
  char *new_buf, *ptr;
  int new_buf_sz;

  /*
   * Calculate size of new buffer needed
   */
  new_buf_sz = *buf_sz + len + ATTR_HDR_SZ;
  new_buf = realloc(buf, new_buf_sz);
  if (!new_buf)
    {
	  err("realloc failed");
	}
  ptr = new_buf + *buf_sz;
  encode_16(ptr, type);
  ptr += 2;
  encode_16(ptr, len);
  ptr += 2;
  memcpy(ptr, value, len);

  *buf_sz = new_buf_sz;
  return new_buf;
}

int
print_attributes (u_int8_t *buf, size_t sz)
{
  u_int8_t *attr;
  int fmt;
  u_int16_t type;
  u_int8_t *value;
  u_int16_t len;
  int i;
  u_int8_t display_buf[MAX_PRINT_BUF_LEN];

  printf("Attributes:\n");
  for (attr = buf; attr < buf + sz; attr = value + len)
    {
      if (attr + 4 > buf + sz)
		return -1;
      type =  decode_16(attr) & 0x7fff;
      fmt = *attr >> 7;
      value = attr + (fmt ? 2 : 4);
      len = (fmt ? 2 : decode_16(attr+2));
      printf("  Format: %d, Type: %03d, Length: %02d Value: ", fmt, type, len);
      if (value + len > buf + sz)
		return -1;
      switch (type) {
	  case SSRC:
	  	printf("SSRC %d (%#x)\n", decode_32(value), decode_32(value));
		break;
	  case DESTINATION_ADDRESS:
	  	printf("Address %#x\n", decode_32(value));
		break;
	  case DESTINATION_RTP_PORT:
	  	printf("RTP Port %d\n", decode_16(value));
		break;
	  case DESTINATION_RTCP_PORT:
	  	printf("RTCP Port %d\n", decode_16(value));
		break;
	  case ROLLOVER_COUNTER:
	  	printf("Rollover Ctr %d\n", decode_32(value));
		break;
	  case CIPHER:
	  	printf("Cipher %d\n", decode_16(value));
		break;
	  case CIPHER_MODE:
	  	printf("Cipher Mode %d\n", decode_16(value));
		break;
	  case CIPHER_KEY_LENGTH:
	  	printf("Cipher Key Length %d\n", decode_16(value));
		break;
	  case SALT_KEY_LENGTH:
	  	printf("Salt Key Length %d\n", decode_32(value));
		break;
	  case AUTHENTICATION_ALGORITHM:
	  	printf("Auth Alg %d\n", decode_16(value));
		break;
	  case REPLAY_WINDOW_SIZE:
	  	printf("Replay Window Sz %d\n", decode_32(value));
		break;
	  case SRTCP:
	  	printf("SRTCP %d\n", decode_16(value));
		break;
	  case SPI:
	  	printf("SPI %d (%#x)\n", decode_32(value), decode_32(value));
		break;
	  case GROUPID_KEYID:
	  	printf("Group ID %d\n", decode_32(value));
		break;
	  case RETURN_PIPE:
		if (len >= MAX_PRINT_BUF_LEN) {
			len = MAX_PRINT_BUF_LEN - 1;
		}
		memcpy(display_buf, value, len);
		display_buf[len] = 0;
	  	printf("Return Pipe %s\n", display_buf);
		break;
	  case CIPHER_KEY:
	  	printf("Cipher Key ");
		for (i=0; i<len; i++)
		  {
			printf("%x", value[i]);
		  }
		printf("\n");
		break;
	  default:
	    printf("Unknown Attribute: %d\n", type);
		break;
	  }
    }
  return 0;
}

void
print_hdr (struct cmd_header *hdr)
{
  printf("  Version:  %d\n", hdr->version);
  printf("  Command:  %d\n", hdr->command);
  printf("  Errno:    %d\n", hdr->peer_errno);
  printf("  Sequence: %d\n", hdr->sequence);
  printf("  Pid:      %d\n", hdr->pid);
  printf("\n");
}

char *create_inital_GET_packet (int *len)
{
  char *buf, *start_attr;
  struct cmd_header *hdr;
  int buf_sz;

  /*
   * Create header. It's a fixed size.
   */
  hdr = calloc(1, sizeof(struct cmd_header));
  if (!hdr)
    {
	  err("calloc failure");
    }
  hdr->version = 1;
  hdr->command = COMMAND_GET;
  srand(time(NULL));
  hdr->sequence = rand();
  hdr->pid = (int) getpid();

  printf("Sending packet:\n");
  print_hdr(hdr);

  buf = (char *) hdr;
  buf_sz = sizeof(struct cmd_header);

  /*
   * Add attributes
   */
  start_attr = buf + buf_sz;
  buf = attribute_add_var(buf, &buf_sz, 
  						  GROUPID_KEYID, default_group.g_char, 4);
  buf = attribute_add_var(buf, &buf_sz, 
  						  RETURN_PIPE, GDOI_CLIENT_PIPE, 
						  strlen(GDOI_CLIENT_PIPE));
 print_attributes(buf + sizeof(struct cmd_header), 
 				  buf_sz - sizeof(struct cmd_header));
  printf("\n");

  *len = buf_sz;
  return buf;
}

void
analyze_returned_GET_packet (char *buf, int len)
{
  struct cmd_header *hdr;

  hdr = (struct cmd_header *) buf;

  printf("Returned Packet:\n");
  print_hdr(hdr);

 print_attributes(buf + sizeof(struct cmd_header), 
 				  len - sizeof(struct cmd_header));
}

int
connect_to_gdoi (void)
{
  int s, ret;
  struct sockaddr_un pipe;

  s = socket (AF_LOCAL, SOCK_STREAM, 0);
  if (s < 0)
    {
	  err("socket open failed");
	  return -1;
    }
  
  bzero(&pipe, sizeof(struct sockaddr_un));
  pipe.sun_family = AF_LOCAL;
  strncpy(pipe.sun_path, SRTP_CLIENT_PIPE, sizeof(pipe.sun_path)-1);

  ret = connect(s, (struct sockaddr *)&pipe, sizeof(pipe));
  if (ret < 0)
    {
	  err("connect failed");
	  return -1;
    }

  return s;
}

int
create_return_sock (void)
{
  int s, ret;
  struct sockaddr_un pipe;

  s = socket (AF_LOCAL, SOCK_STREAM, 0);
  if (s < 0)
    {
	  err("socket open failed");
	  return;
    }

  unlink(GDOI_CLIENT_PIPE);
  
  bzero(&pipe, sizeof(struct sockaddr_un));
  pipe.sun_family = AF_LOCAL;
  strncpy(pipe.sun_path, GDOI_CLIENT_PIPE, sizeof(pipe.sun_path)-1);

  ret = bind(s, (struct sockaddr *)&pipe, sizeof(pipe));
  if (ret < 0)
    {
	  err("bind failed");
	  return;
    }

  ret = listen(s, 1024);
  if (ret < 0)
    {
	  err("listen failed");
	  return;
    }

  return s;
}

main (argc, argv)
int argc;
char **argv;
{
  int s_to_gdoi, s_from_gdoi, c;
  int ret;
  char *data_out;
  char data_in[1024];
  int data_in_len, data_out_len;
  int cc;
  char *usage="-g <group_number>";

  struct msghdr msg;
  struct iovec iov[1];
  union {
    struct cmsghdr	cm;
    char				control[CMSG_SPACE(sizeof(int))];
  } control_un;
  struct cmsghdr	*cmptr;

  struct sockaddr_un from;
  int from_len;

  /*
   * Option processing 
   */
  while (1)
    {
  	  cc = getopt(argc, argv, "g:");
  	  if (cc == -1)
	    {
		  break;
		}
	  switch (cc)
	    {
		case 'g':
  		  default_group.g = htonl(atoi(optarg));
  		  /* default_group.g = htonl(*optarg);*/
		  break;
		default:
		  printf("Unknown option %c\n", cc);
		  printf("Usage: %s %s\n", argv[0], usage);
		  exit(1);
		}
	}
  if (!default_group.g)
    {
	  printf("Usage: %s %s\n", argv[0], usage);
	  exit(1);
	}
  s_to_gdoi = connect_to_gdoi();
  if (s_to_gdoi < 0)
    {
	  return;
    }

  s_from_gdoi = create_return_sock();

  data_out = create_inital_GET_packet(&data_out_len);

  msg.msg_name = NULL;
  msg.msg_namelen = 0;
  iov[0].iov_base = data_out;
  iov[0].iov_len = data_out_len;
  msg.msg_control = 0;
  msg.msg_controllen = 0;
  msg.msg_iov = iov;
  msg.msg_iovlen = 1;

  ret = sendmsg(s_to_gdoi, &msg, 0);
  if (ret < 0)
    {
	  err("sendmsg failed");
	  return;
    }

  c = accept(s_from_gdoi, (struct sockaddr *)&from, &from_len);
  if (c < 0)
    {
	  err("accept failed");
	  return;
    }

  /*
   * Look for return messages. Two types possible:
   * a. Returns to the GET packet send above. If there are multiple
   *    TEKs they will be sent in different messages.
   * b. Subsequent ADD packets sent by GDOI to tell us about new SAs.
   */

  while (1)
    {
	
	  ret = recvfrom(c, &data_in, MAX_MSG_SIZE, 0, NULL, NULL);
	  if (ret < 0)
	    {
		  err("recvfrom failed");
		  return;
	    }

	  if (ret)
	    {
	      analyze_returned_GET_packet(data_in, ret);
		}
	  else
	  	{
		  printf("\nGDOI closed the connection\n");
		  exit(0);
		}
    }
}
