# $Id: gdoi_ks.conf,v 1.4 2003/08/15 23:23:13 bew Exp $
# $Source: /nfs/cscbz/gdoi/gdoicvs/gdoi/samples/three-clients/gdoi_ks.conf,v $

# 
# A configuration sample for testing GDOI between systems passing IPSec policy.
# This is an example of the key server.
#

[General]
Retransmits=		5
Exchange-max-time=	120
Listen-on=		128.178.151.151

# Incoming phase 1 negotiations are multiplexed on the source IP address
[Phase 1]
172.19.216.230=		GDOI-group-member-1
172.19.216.232=		GDOI-group-member-2
172.19.216.228=		GDOI-group-member-3

# These connections are walked over after config file parsing and told
# to the application layer so that it will inform us when traffic wants to
# pass over them.  This means we can do on-demand keying.
[Phase 2]
Passive-Connections=		IPsec-group-policy

[GDOI-group-member-1]
Phase=			1
Transport=		udp
Local-address=		128.178.151.151
Address=		172.19.216.230
# Default values for "Port" commented out
#Port=			isakmp
Port=			500
#Port=			5001
Configuration=		Default-main-mode
Authentication=		mekmitasdigoat

[GDOI-group-member-2]
Phase=			1
Transport=		udp
Local-address=		172.19.216.229
Address=		172.19.216.232
# Default values for "Port" commented out
#Port=			isakmp
Port=			500
#Port=			5001
Configuration=		Default-main-mode
Authentication=		mekmitasdigoat

[GDOI-group-member-3]
Phase=			1
Transport=		udp
Local-address=		172.19.216.229
Address=		172.19.216.228
# Default values for "Port" commented out
#Port=			isakmp
Port=			500
#Port=			5001
Configuration=		Default-main-mode
Authentication=		mekmitasdigoat

[IPsec-group-policy]
Phase=			2
ISAKMP-peer=		GDOI-group-member
Configuration=		Default-group-mode
Group-ID=		Group-1

[Group-1]
ID-type=		KEY_ID
Key-value=		1234

# Main mode descriptions

[Default-main-mode]
DOI=			GROUP
EXCHANGE_TYPE=		ID_PROT
Transforms=		3DES-SHA

# Main mode transforms
######################

# DES

[DES-MD5]
ENCRYPTION_ALGORITHM=	DES_CBC
HASH_ALGORITHM=		MD5
AUTHENTICATION_METHOD=	PRE_SHARED
GROUP_DESCRIPTION=	MODP_768
Life=			LIFE_600_SECS

[DES-MD5-NO-VOL-LIFE]
ENCRYPTION_ALGORITHM=	DES_CBC
HASH_ALGORITHM=		MD5
AUTHENTICATION_METHOD=	PRE_SHARED
GROUP_DESCRIPTION=	MODP_768
Life=			LIFE_600_SECS

[DES-SHA]
ENCRYPTION_ALGORITHM=	DES_CBC
HASH_ALGORITHM=		SHA
AUTHENTICATION_METHOD=	PRE_SHARED
GROUP_DESCRIPTION=	MODP_768
Life=			LIFE_600_SECS

# 3DES

[3DES-SHA]
ENCRYPTION_ALGORITHM=	3DES_CBC
HASH_ALGORITHM=		SHA
AUTHENTICATION_METHOD=	PRE_SHARED
GROUP_DESCRIPTION=	MODP_1024
Life=			LIFE_3600_SECS

# Lifetimes

[LIFE_600_SECS]
LIFE_TYPE=		SECONDS
LIFE_DURATION=		600,450:720

[LIFE_3600_SECS]
LIFE_TYPE=		SECONDS
LIFE_DURATION=		3600,1800:7200

[LIFE_1000_KB]
LIFE_TYPE=		KILOBYTES
LIFE_DURATION=		1000,768:1536

[LIFE_32_MB]
LIFE_TYPE=		KILOBYTES
LIFE_DURATION=		32768,16384:65536

[LIFE_4.5_GB]
LIFE_TYPE=		KILOBYTES
LIFE_DURATION=		4608000,4096000:8192000

# Quick Mode description
########################

# 3DES

[QM-ESP-3DES-SHA-XF-BEW]
TRANSFORM_ID=		3DES
ENCAPSULATION_MODE=	TUNNEL
AUTHENTICATION_ALGORITHM=	HMAC_SHA
Life=			LIFE_600_SECS

# Group mode description
########################

[Default-group-mode]
DOI=			GROUP
EXCHANGE_TYPE=		PULL_MODE
SA-KEK=			GROUP1-KEK
SA-TEKS=		GROUP1-TEK1,GROUP1-TEK2

[GROUP1-KEK]
Src-ID=               	Group-kek-src
Dst-ID=              	Group-kek-dst
SPI=			abcdefgh01234567
ENCRYPTION_ALGORITHM=	3DES
SIG_HASH_ALGORITHM=	SHA
SIG_ALGORITHM=		RSA
DES_IV=			IVIVIVIV
DES_KEY1=		ABCDEFGH
DES_KEY2=		IJKLMNOP
DES_KEY3=		QRSTUVWX
RSA-Keypair=		/usr/local/gdoid/rsakeys.der
REKEY_PERIOD=		30

[Group-kek-src]
ID-type=                IPV4_ADDR
Address=                172.19.216.229
Port=			5001

[Group-kek-dst]
ID-type=                IPV4_ADDR
Address=                239.1.1.1
Port=			5001

# Src-ID and Dst-ID are the addresses for the IP ESP packet.
[GROUP1-TEK1]
Crypto-protocol=	PROTO_IPSEC_ESP
Src-ID=               	Group-tek1-src
Dst-ID=              	Group-tek1-dst
# SPI is 0x1122aabb
SPI=			287484603
TEK_Suite=		QM-ESP-3DES-SHA-SUITE-BEW
DES_KEY1=		ABCDEFGH
DES_KEY2=		IJKLMNOP
DES_KEY3=		QRSTUVWX
SHA_KEY=		12345678901234567890

[Group-tek1-src]
ID-type=                IPV4_ADDR
Address=                172.19.193.37
Port=			1024

[Group-tek1-dst]
ID-type=                IPV4_ADDR
Address=                239.192.1.1
Port=			1024

# Src-ID and Dst-ID are the addresses for the IP ESP packet.
[GROUP1-TEK2]
Src-ID=               	Group-tek2-src
Dst-ID=              	Group-tek2-dst
# SPI is 0x3344ccdd
SPI=			860146909
TEK_Suite=		QM-ESP-3DES-SHA-SUITE-BEW
DES_KEY1=		FEDCBA11
DES_KEY2=		LKJIHG22
DES_KEY3=		RQPONM33
SHA_KEY=		01234567890123456789

[Group-tek2-src]
ID-type=                IPV4_ADDR
Address=                172.19.193.38
Port=			1024

[Group-tek2-dst]
ID-type=                IPV4_ADDR
Address=                239.192.1.1
Port=			1024

[QM-ESP-3DES-SHA-SUITE-BEW]
Protocols=              QM-ESP-3DES-SHA

[QM-ESP-3DES-SHA-BEW]
PROTOCOL_ID=            IPSEC_ESP
Transforms=             QM-ESP-3DES-SHA-XF-BEW

# Certificates stored in PEM format
[X509-certificates]
CA-directory=		/etc/gdoid/ca/
Cert-directory=		/etc/gdoid/certs/
#Accept-self-signed=	defined
Private-key=		/etc/gdoid/private/local.key
