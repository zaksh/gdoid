# $Id: gdoi_ks.conf,v 1.3 2003/08/15 23:23:00 bew Exp $
# $Source: /nfs/cscbz/gdoi/gdoicvs/gdoi/samples/loopback/gdoi_ks.conf,v $

# 
# A configuration sample for testing GDOI over loopback interfaces.
# This is the key server side.
#

[General]
Retransmits=		5
Exchange-max-time=	120
Listen-on=		127.0.0.2

# Incoming phase 1 negotiations are multiplexed on the source IP address
[Phase 1]
127.0.0.1=		ISAKMP-peer-client

# These connections are walked over after config file parsing and told
# to the application layer so that it will inform us when traffic wants to
# pass over them.  This means we can do on-demand keying.
[Phase 2]
Passive-Connections=		Group-1234

[ISAKMP-peer-client]
Phase=			1
Transport=		udp
Local-address=		127.0.0.2
Address=		127.0.0.1
Configuration=		Default-main-mode
Authentication=		mekmitasdigoat

[Group-1234]
Phase=			2
Configuration=		Default-group-mode
Group-ID=		Group-1

[Group-1]
ID-type=		KEY_ID
Key-value=		1234

# Main mode descriptions

[Default-main-mode]
DOI=			GROUP
EXCHANGE_TYPE=		ID_PROT
Transforms=		3DES-SHA

# Main mode transforms
######################

# 3DES

[3DES-SHA]
ENCRYPTION_ALGORITHM=	3DES_CBC
HASH_ALGORITHM=		SHA
AUTHENTICATION_METHOD=	PRE_SHARED
GROUP_DESCRIPTION=	MODP_1024
Life=			LIFE_3600_SECS

# Lifetimes

[LIFE_3600_SECS]
LIFE_TYPE=		SECONDS
LIFE_DURATION=		3600,1800:7200

# GDOI description
########################

# 3DES

[GDOI-ESP-3DES-SHA-XF]
TRANSFORM_ID=		3DES
ENCAPSULATION_MODE=	TUNNEL
AUTHENTICATION_ALGORITHM=	HMAC_SHA
Life=			LIFE_3600_SECS

# Group mode description
########################

[Default-group-mode]
DOI=			GROUP
EXCHANGE_TYPE=		PULL_MODE
# 
# No SA-KEK is defined for the loopback sample.
# Rekey messages don't always work across the loopbacks.
#
SA-TEKS=		GROUP1-TEK1,GROUP1-TEK2

# Src-ID and Dst-ID are the addresses for the IP ESP packet.
[GROUP1-TEK1]
Crypto-protocol=	PROTO_IPSEC_ESP
Src-ID=               	Group-tek1-src
Dst-ID=              	Group-tek1-dst
# SPI is 0x1122aabb
SPI=			287484603
TEK_Suite=		GDOI-ESP-3DES-SHA-SUITE
DES_KEY1=		ABCDEFGH
DES_KEY2=		IJKLMNOP
DES_KEY3=		QRSTUVWX
SHA_KEY=		12345678901234567890

[Group-tek1-src]
ID-type=                IPV4_ADDR
Address=                172.19.137.42
Port=			1024

[Group-tek1-dst]
ID-type=                IPV4_ADDR
Address=                239.192.1.1
Port=			1024

# Src-ID and Dst-ID are the addresses for the IP ESP packet.
[GROUP1-TEK2]
Src-ID=               	Group-tek2-src
Dst-ID=              	Group-tek2-dst
# SPI is 0x3344ccdd
SPI=			860146909
TEK_Suite=		GDOI-ESP-3DES-SHA-SUITE
DES_KEY1=		FEDCBA11
DES_KEY2=		LKJIHG22
DES_KEY3=		RQPONM33
SHA_KEY=		01234567890123456789

[Group-tek2-src]
ID-type=                IPV4_ADDR
Address=                172.19.137.42
Port=			512

[Group-tek2-dst]
ID-type=                IPV4_ADDR
Address=                239.192.1.2
Port=			512

[GDOI-ESP-3DES-SHA-SUITE]
Protocols=              GDOI-ESP-3DES-SHA

[GDOI-ESP-3DES-SHA]
PROTOCOL_ID=            IPSEC_ESP
Transforms=             GDOI-ESP-3DES-SHA-XF

# Certificates stored in PEM format
[X509-certificates]
CA-directory=		/etc/gdoid/ca/
Cert-directory=		/etc/gdoid/certs/
#Accept-self-signed=	defined
Private-key=		/etc/gdoid/private/local.key
