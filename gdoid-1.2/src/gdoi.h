/* $Id: gdoi.h,v 1.6 2003/07/25 04:06:06 bew Exp $ */
/* $Source: /nfs/cscbz/gdoi/gdoicvs/gdoi/src/gdoi.h,v $ */

/* 
 * The license applies to all software incorporated in the "Cisco GDOI reference
 * implementation" except for those portions incorporating third party software 
 * specifically identified as being licensed under separate license. 
 *  
 *  
 * The Cisco Systems Public Software License, Version 1.0 
 * Copyright (c) 2001-2002 Cisco Systems, Inc. All rights reserved.
 * Subject to the following terms and conditions, Cisco Systems, Inc., 
 * hereby grants you a worldwide, royalty-free, nonexclusive, license, 
 * subject to third party intellectual property claims, to create 
 * derivative works of the Licensed Code and to reproduce, display, 
 * perform, sublicense, distribute such Licensed Code and derivative works. 
 * All rights not expressly granted herein are reserved. 
 * 1.      Redistributions of source code must retain the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer.
 * 2.      Redistributions in binary form must reproduce the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 3.      The names Cisco and "Cisco GDOI reference implementation" must not 
 * be used to endorse or promote products derived from this software without 
 * prior written permission. For written permission, please contact 
 * opensource@cisco.com.
 * 4.      Products derived from this software may not be called 
 * "Cisco" or "Cisco GDOI reference implementation", nor may "Cisco" or 
 * "Cisco GDOI reference implementation" appear in 
 * their name, without prior written permission of Cisco Systems, Inc.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE, TITLE AND NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT 
 * SHALL CISCO SYSTEMS, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF 
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. THIS LIMITATION OF LIABILITY SHALL NOT APPLY TO 
 * LIABILITY FOR DEATH OR PERSONAL INJURY RESULTING FROM SUCH 
 * PARTY'S NEGLIGENCE TO THE EXTENT APPLICABLE LAW PROHIBITS SUCH 
 * LIMITATION. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR 
 * LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THAT 
 * EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU. FURTHER, YOU 
 * AGREE THAT IN NO EVENT WILL CISCO'S LIABILITY UNDER OR RELATED TO 
 * THIS AGREEMENT EXCEED AMOUNT FIVE THOUSAND DOLLARS (US) 
 * (US$5,000). 
 *  
 * ====================================================================
 * This software consists of voluntary contributions made by Cisco Systems, 
 * Inc. and many individuals on behalf of Cisco Systems, Inc. For more 
 * information on Cisco Systems, Inc., please see <http://www.cisco.com/>.
 *
 * This product includes software developed by Ericsson Radio Systems.
 */ 

#ifndef _GDOI_H_
#define _GDOI_H_
#include <netinet/in.h>
#include <hash.h>
#include "transport.h"
#ifdef USE_X509
#include <openssl/rsa.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/evp.h>
#include <openssl/x509.h>
#include <openssl/pem.h>
#endif

#define KEK_SPI_SIZE 16 

#define FALSE 0
#define TRUE  1

/*
 * Group KEK in-memory structure.
 */
struct gdoi_kek {
  TAILQ_ENTRY (gdoi_kek) link;
  u_int8_t *group_id;
  u_int32_t group_id_len;
  in_addr_t src_addr;
  in_addr_t dst_addr;
  u_int16_t sport;
  u_int16_t dport;
  u_int8_t spi[KEK_SPI_SIZE];
  u_int32_t current_seq_num;
  u_int32_t replay_bitmap;
  u_int16_t encrypt_alg;
  u_int16_t sig_hash_alg;
  u_int16_t sig_alg;
  u_int8_t *encrypt_iv;
  u_int8_t *encrypt_key; /* 3DES keys are stored as one value */
  u_int8_t *signature_key;
  u_int32_t signature_key_len;
#ifdef USE_X509
  RSA *rsa_keypair;
#endif
  u_int32_t timer_interval;
  struct event *ev;
  int recv_sock;
  int send_sock;
  struct transport *send_transport;
  struct exchange *send_exchange;
  struct sockaddr_in recv_addr; /* Sender socket to join group */
  struct sockaddr_in send_addr; /* Sender socket to send to group */
  char *exchange_name;
  struct ip_mreq mreq;
};

extern int (*gdoi_rekey_initiator[]) (struct message *);
extern int (*gdoi_rekey_responder[]) (struct message *);

void gdoi_rekey_init(void);
struct gdoi_kek *gdoi_get_kek (u_int8_t *, size_t, int);
int gdoi_read_keypair (u_int8_t *, struct gdoi_kek *);
int gdoi_store_pubkey (u_int8_t *, int, struct gdoi_kek *);
int gdoi_rekey_start (struct gdoi_kek *, int);
int gdoi_rekey_listen (struct gdoi_kek *);
int gdoi_rekey_setup_exchange (struct gdoi_kek *);
struct gdoi_kek *gdoi_get_kek_by_cookies (u_int8_t *);
struct gdoi_kek *gdoi_get_kek_by_transport (struct transport *);
struct gdoi_kek *gdoi_get_kek_by_name (char *);

enum hashes xlate_gdoi_hash (u_int16_t);

#endif /* _GDOI_H_ */
