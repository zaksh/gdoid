/* $Id: gdoi_srtp.c,v 1.5 2003/08/15 23:24:03 bew Exp $ */
/* $Source: /nfs/cscbz/gdoi/gdoicvs/gdoi/src/gdoi_srtp.c,v $ */

/* 
 * The license applies to all software incorporated in the "Cisco GDOI reference
 * implementation" except for those portions incorporating third party software 
 * specifically identified as being licensed under separate license. 
 *  
 *  
 * The Cisco Systems Public Software License, Version 1.0 
 * Copyright (c) 2001 Cisco Systems, Inc. All rights reserved.
 * Subject to the following terms and conditions, Cisco Systems, Inc., 
 * hereby grants you a worldwide, royalty-free, nonexclusive, license, 
 * subject to third party intellectual property claims, to create 
 * derivative works of the Licensed Code and to reproduce, display, 
 * perform, sublicense, distribute such Licensed Code and derivative works. 
 * All rights not expressly granted herein are reserved. 
 * 1.      Redistributions of source code must retain the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer.
 * 2.      Redistributions in binary form must reproduce the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 3.      The names Cisco and "Cisco GDOI reference implementation" must not 
 * be used to endorse or promote products derived from this software without 
 * prior written permission. For written permission, please contact 
 * opensource@cisco.com.
 * 4.      Products derived from this software may not be called 
 * "Cisco" or "Cisco GDOI reference implementation", nor may "Cisco" or 
 * "Cisco GDOI reference implementation" appear in 
 * their name, without prior written permission of Cisco Systems, Inc.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE, TITLE AND NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT 
 * SHALL CISCO SYSTEMS, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF 
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. THIS LIMITATION OF LIABILITY SHALL NOT APPLY TO 
 * LIABILITY FOR DEATH OR PERSONAL INJURY RESULTING FROM SUCH 
 * PARTY'S NEGLIGENCE TO THE EXTENT APPLICABLE LAW PROHIBITS SUCH 
 * LIMITATION. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR 
 * LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THAT 
 * EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU. FURTHER, YOU 
 * AGREE THAT IN NO EVENT WILL CISCO'S LIABILITY UNDER OR RELATED TO 
 * THIS AGREEMENT EXCEED AMOUNT FIVE THOUSAND DOLLARS (US) 
 * (US$5,000). 
 *  
 * ====================================================================
 * This software consists of voluntary contributions made by Cisco Systems, 
 * Inc. and many individuals on behalf of Cisco Systems, Inc. For more 
 * information on Cisco Systems, Inc., please see <http://www.cisco.com/>.
 *
 * This product includes software developed by Ericsson Radio Systems.
 */ 


#include <stdlib.h>
#include <string.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#include "attribute.h"
#include "conf.h"
#include "connection.h"
#include "doi.h"
#include "exchange.h"
#include "hash.h"
#include "gdoi_phase2.h"
#include "log.h"
#include "math_group.h"
#include "message.h"
#include "prf.h"
#include "sa.h"
#include "transport.h"
#include "util.h"
#include "gdoi_fld.h"
#include "gdoi_num.h"
#include "gdoi_srtp.h"
#include "gdoi_srtp_num.h"
#include "ipsec_num.h"
#include "gdoi.h"

#define AES_LENGTH 16

#define ATTR_SIZE (50 * ISAKMP_ATTR_VALUE_OFF)
      
/*
 * Key server side
 * Find the TEK-specific policy for an SRTP type TEK.
 */
int gdoi_srtp_get_policy (char *conf_field, u_int8_t **ret_buf, 
			  size_t *ret_buf_sz, struct message *msg,
			  struct exchange *sa_exchange)
{
  struct exchange *exchange = msg->exchange;
  struct sa *sa;
  u_int8_t *srtp_tek_buf = 0;
  u_int8_t *buf = 0;
  size_t srtp_tek_sz;
  u_int8_t *attr, *attr_start;
  char *ssrc, *dst_id, *spi;
  u_int8_t *key_str;
  u_int8_t *key;
  u_int32_t srtp_spi;
  struct proto *proto;
  struct srtp_proto *sproto;
  struct in_addr addr;
  struct in_addr mask;
  uint16_t port = 0;
  int id_type = 0;

  /*
   * Find the sa. The last SA in the list was just created for our use.
   */
  sa = TAILQ_LAST (&sa_exchange->sa_list, sa_head);
  if (!sa)
	{
   	  log_error ("gdoi_ipsec_get_policy: No sa's in list!");
   	  goto bail_out;
	}
 
  /*
   * Add the SPI to the proto structure for the KD payload later.
   * This requires setting up a proto
   * structure and filling it in with the relevant info (SPIs and keys).
   */
  if (gdoi_setup_sa (sa, &proto, SRTP_PROTO_SRTP, sizeof(struct srtp_proto)))
	{
	  goto bail_out;
	}
  sproto = proto->data;

  /*
   * Start with getting the SSRC (must be 32 bits).
   */
  if (exchange->type == GDOI_EXCH_PUSH_MODE)
	{
	  getrandom((u_int8_t *)&sproto->ssrc, SRTP_SSRC_SIZE);
	}
  else
	{
  	  ssrc = conf_get_str (conf_field, "SSRC");
  	  if (!ssrc) 
    	{
      	  log_print ("gdoi_srtp_get_policy: SSRC missing");
  	  	  goto bail_out;
    	}
  	  sproto->ssrc = htonl(atoi(ssrc));
	}
  srtp_tek_sz = SRTP_SSRC_SIZE;
  srtp_tek_buf = malloc(srtp_tek_sz);
  if (!srtp_tek_buf)
    {
      log_print ("gdoi_srtp_get_policy: malloc failed: SSRC");
  	  goto bail_out;
    }
  memcpy(srtp_tek_buf, &sproto->ssrc, srtp_tek_sz);

  /*
   * Get the SPI for this TEK.
   */
  proto->spi_sz[0]=SRTP_SPI_SIZE;
  proto->spi[0] = calloc(1, proto->spi_sz[0]);
  if (!proto->spi[0])
    {
      log_print ("gdoi_srtp_get_policy: "
       		   	 "calloc failed (%d)", proto->spi_sz[0]);
      goto bail_out;
    }
  if (exchange->type == GDOI_EXCH_PUSH_MODE)
	{
	  getrandom(proto->spi[0], SRTP_SPI_SIZE);
	}
  else
	{
	  spi = conf_get_str (conf_field, "SPI");
  	  if (!spi) 
    	{
  	  	  goto bail_out;
    	}
  	  srtp_spi = htonl(atoi(spi));
  	  memcpy(proto->spi[0], &srtp_spi, proto->spi_sz[0]);
	}
  srtp_tek_buf = gdoi_grow_buf(srtp_tek_buf, &srtp_tek_sz, 
							   proto->spi[0], SRTP_SPI_SIZE);
  if (!srtp_tek_buf)
    {
      goto bail_out;
  }


  /*
   * BEGIN ATTRIBUTE PROCESSING
   * Allocate a block for building attributes. It's sized large enough
   * so that we think it will avoid buffer overflows....
   */
  attr_start = attr = calloc(1, ATTR_SIZE); 
  if (!attr)
    {
  	  log_print ("gdoi_srtp_get_policy: "
              	 "calloc(%d) failed", ATTR_SIZE);
  	  goto bail_out;
	}

  /*
   * Get Destination Address
   */
  dst_id = conf_get_str (conf_field, "Dst-ID");
  if (!dst_id)
    {
      log_print ("gdoi_ipsec_get_policy: Dst-ID missing");
	  goto bail_out;
    }
  if (gdoi_get_id (dst_id, &id_type, &addr, &mask, &port))
    {
      log_print ("gdoi_srtp_get_policy: Dst-ID missing");
  	  goto bail_out;
    }
  if ((id_type != IPSEC_ID_IPV4_ADDR) || !addr.s_addr || !port)
    {
      log_print ("gdoi_srtp_get_policy: Dst-ID invalid");
  	  goto bail_out;
	}
  attr = attribute_set_var (attr, SRTP_ATTR_DESTINATION_ADDRESS,
  							(u_int8_t *) &addr.s_addr, sizeof(addr.s_addr));
  attr = attribute_set_var (attr, SRTP_ATTR_DESTINATION_RTP_PORT,
  							(u_int8_t *) &port, sizeof(port));

  /*
   * Save the keys for the KD payload. 
   */

  /*
   * Look for key. Assume AES in counter mode.
   */
  sproto->cipher_type = SRTP_CIPHER_AES;
  sproto->cipher_mode = SRTP_CIPHER_MODE_COUNTER_MODE;
  sproto->cipher_key_length = AES_LENGTH;
  sproto->cipher_key = calloc(1, sproto->cipher_key_length);
  if (!sproto->cipher_key)
    {
      log_print ("gdoi_srtp_get_policy: calloc failed (%d)",
		 sproto->cipher_key_length);
      goto bail_out;
  	}
  if (exchange->type == GDOI_EXCH_PUSH_MODE)
	{
	  getrandom(sproto->cipher_key, sproto->cipher_key_length);
	}
  else
	{
  	  key_str = "AES_KEY";
  	  key = conf_get_str (conf_field, key_str);
  	  if (!key)
    	{
   	  	  log_print ("gdoi_srtp_get_policy: %s key not found.", key_str);
      	  goto bail_out;
		}
  	  memcpy(sproto->cipher_key, key, sproto->cipher_key_length);
	}

  /*
   * Put the cipher into the payload as attributes
   */
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER, sproto->cipher_type);
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER_MODE,
  							  		sproto->cipher_mode);
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER_KEY_LENGTH,
  							  		sproto->cipher_key_length);
  /*
   * Add the attributes to the tek payload
   */
  srtp_tek_buf = gdoi_grow_buf(srtp_tek_buf, &srtp_tek_sz, attr_start, 
                               (attr - attr_start));
  free (attr_start);
  if (!srtp_tek_buf)
    {
      goto bail_out;
	}

  *ret_buf = srtp_tek_buf;
  *ret_buf_sz = srtp_tek_sz;

  /*
   * Save the SRTP policy in the sa for later use.
   */
  if (gdoi_srtp_decode_tek(msg, sa, srtp_tek_buf, srtp_tek_sz, FALSE))
  	{
	  log_print ("gdoi_ipsec_get_policy: "
	 		   	 "Failed to copy TEK policy into SA");
      goto bail_out;
	}

  /*
   * Add the SPI to the exchange list for use of the KD payload processing.
   */
  if (gdoi_add_spi_to_list(exchange, sa))
    {
	  goto bail_out;
	}
   
  return 0;

bail_out:
    if (buf)
      {
        free (buf);
      }
    gdoi_free_attr_payloads();
    return -1;
}

int srtp_decode_attribute (u_int16_t type, u_int8_t *value, u_int16_t len,
						   void *arg)
{
  struct srtp_proto *sa = (struct srtp_proto *) arg;

  switch (type)
    {
	case SRTP_ATTR_DESTINATION_ADDRESS:
	  sa->addr.s_addr = decode_32(value);
	  break;
	case SRTP_ATTR_DESTINATION_RTP_PORT:
	  sa->port = decode_16(value);
	  break;
	case SRTP_ATTR_CIPHER:
	  sa->cipher_type = decode_16(value);
	  break;
	case SRTP_ATTR_CIPHER_MODE:
	  sa->cipher_mode = decode_16(value);
	  break;
	case SRTP_ATTR_CIPHER_KEY_LENGTH:
	  sa->cipher_key_length = decode_16(value);
	  break;
	case SRTP_ATTR_ROLLOVER_COUNTER:
	case SRTP_ATTR_SALT_KEY_LENGTH:
	case SRTP_ATTR_AUTHENTICATION_ALGORITHM:
	case SRTP_ATTR_REPLAY_WINDOW_SIZE:
	case SRTP_ATTR_SRTCP:
      log_print ("srtp_decode_attribute: Attribute not supported: %d", type);
	  return -1;
	default:
      log_print ("srtp_decode_attribute: Attribute not valid: %d", type);
	  return -1;
	}

  return 0;
}

/*
 * Group member side
 * Also executed on the key server side to save a copy of the SA in his own
 *   sa list for later use by the rekey message.
 * Decode the SRTP type TEK and stuff into the SA.
 */
int
gdoi_srtp_decode_tek (struct message *msg, struct sa *sa, u_int8_t *srtp_tek,
			  		  size_t srtp_tek_len, int create_proto)
{
  u_int8_t *cur_p;
  int temp_len;
  struct proto *proto = NULL;
  struct srtp_proto *sproto = NULL;
  
  /*
   * Validate the SA.
   */
  if (!sa)
    {
  	  log_error ("group_decode_esp_tek: No sa's in list!");
  	  goto clean_up;
	}

  if (create_proto)
  	{
  	  if (gdoi_setup_sa (sa, &proto, SRTP_PROTO_SRTP,
						 sizeof(struct srtp_proto)))
		{
	  	  goto clean_up;
		}
	}
  else
    {
	  proto = TAILQ_LAST(&sa->protos, proto_head);
	}

  /*
   * Stuff the SRTP policy in the proto structure. (Can't use sa->data because
   * that is initialized in sa_create(). sa->data is unused for SRTP.)
   */
  sproto = (struct srtp_proto *) proto->data;

  /*
   * Interpret the SRTP TEK header
   *  Get SSRC
   */
  
  cur_p = srtp_tek;
  sproto->ssrc = ntohl(decode_32(cur_p));
  cur_p += sizeof(u_int32_t);

  /*
   * Get SPI
   */
  proto->spi_sz[0]=SRTP_SPI_SIZE;
  proto->spi[0] = calloc(1, proto->spi_sz[0]);
  if (!proto->spi[0])
    {
      log_print ("gdoi_srtp_decode_tek: "
       		   	 "calloc failed (%d)", proto->spi_sz[0]);
      goto clean_up;
    }
  memcpy(proto->spi[0], cur_p, SRTP_SPI_SIZE);
  log_print(" SPI found (SA) %ud (%#x) for sa %#x", decode_32(proto->spi[0]), 
			decode_32(proto->spi[0]), sa);

  cur_p += SRTP_SPI_SIZE;

  temp_len = srtp_tek_len - (cur_p - srtp_tek);

  attribute_map (cur_p, temp_len, srtp_decode_attribute, sproto);

  /*
   * HACK! There is no time boundary for the SRTP SAs, but in order to
   *       be efficient we need to impose one. Hard code a time limit for now.
   *       The time should really be linked to the GROUPKEY-PUSH period for
   *       the group, or zero if there is no KEK.
   */
  if (!sa->seconds)
	{ 
 	  sa->seconds = 60; /* One minute */
	}

  return 0;
 
clean_up:
  if (proto)
    {
  	  proto_free(proto);
	}
  return -1;
}

/*
 * Group member side
 * Match a KD payload to to an SA, and install the keys there.
 */
int
gdoi_srtp_install_keys (struct sa *sa, u_int8_t *key_packet)
{
  struct proto *proto;
  struct srtp_proto *sproto;

  size_t kd_spi_sz;
  u_int8_t *kd_spi;
  struct gdoi_kd_decode_arg *keys;
  u_int8_t *attr_p;
  size_t attr_len;
				
  proto = TAILQ_FIRST (&sa->protos);
  if (!proto)
    {
      log_error ("gdoi_srtp_install_keys: SRTP SA proto data missing");
      return -1;
    }
  if (proto->proto != SRTP_PROTO_SRTP)
    {
      log_error ("gdoi_srtp_install_keys: SRTP SA expected, got %d",
	  			 proto->proto);
      return -1;
	}

  sproto = (struct srtp_proto *) proto->data;
  if (!sproto)
    {
      log_error ("gdoi_srtp_install_keys: SRTP SA TEK data missing");
      return -1;
    }

  /*
   * Compare the SPI in the packet to SPI in the TEK
   */
  kd_spi_sz = GET_GDOI_KD_PAK_SPI_SIZE(key_packet);
  if (kd_spi_sz != SRTP_SPI_SIZE)
    {
 	  log_print ("gdoi_srtp_install_tek_keys: "
			     "Unsupported spi size: %d", kd_spi_sz);
	  return -1;
	}
  kd_spi = key_packet + GDOI_KD_PAK_SPI_SIZE_OFF + GDOI_KD_PAK_SPI_SIZE_LEN;
  if (memcmp(proto->spi[0], kd_spi, SRTP_SPI_SIZE))
	{
	  /* No match. Try the next one */
	  return 0;
	}
  /*
   * SPIs match!
   */
  log_print(" SPI found (KD) %ud (%#x) for sa %#x",
			 decode_32(kd_spi), decode_32(kd_spi), sa);

  /*
   * Get the keys from the key packet.
   */
  keys = calloc(1, sizeof(struct gdoi_kd_decode_arg));
  if (!keys)
	{
  	  log_error ("gdoi_srtp_install_tek_keys: calloc failed - keys");
					
	}

  attr_p = key_packet + GDOI_KD_PAK_SPI_SIZE_OFF + GDOI_KD_PAK_SPI_SIZE_LEN +
		   kd_spi_sz;
  attr_len = GET_GDOI_KD_PAK_LENGTH(key_packet) - GDOI_KD_PAK_SPI_SIZE_LEN - 
	     kd_spi_sz;
  attribute_map (attr_p, attr_len, gdoi_decode_kd_tek_attribute, (void *)keys);

  /*
   * Validate that the key length is correct & copy them.
   */
  if (keys->sec_key_sz != sproto->cipher_key_length)
    {
	  log_error ("gdoi_srtp_install_tek_keys:"
	  			 "Wrong key length! Expected: %d, Actual: %d",
				 sproto->cipher_key_length, keys->sec_key_sz);
	  return -1;
	}

  sproto->cipher_key = keys->sec_key;  

  free(keys);

  return 0;
}

/*
 * Group member side
 * Finalize the exchange -- send the key & policy info to the SRTP app.
 */
int
gdoi_srtp_deliver_keys (struct message *msg, struct sa *sa)
{
  /*
   * Give the keys to the client s/w.
   */
  srtp_deliver_keys (sa);
  return 0;
}

/*
 * Translate keys from the SRTP proto into a generic structure
 */
int
gdoi_srtp_get_tek_keys (struct gdoi_kd_decode_arg *keys, struct proto *proto)
{
 struct srtp_proto *sproto= (struct srtp_proto *) proto->data;

  keys->sec_key_sz = sproto->cipher_key_length;
  keys->int_key_sz = 0;

  if (keys->sec_key_sz)
  	{
  	  keys->sec_key = malloc(keys->sec_key_sz);
  	  if (!keys->sec_key)
  		{
	  	  return -1;
		}
  	  memcpy(keys->sec_key, sproto->cipher_key, keys->sec_key_sz);
	}
 
  if (keys->int_key_sz)
  	{
  	  keys->int_key = malloc(keys->int_key_sz);
  	  if (!keys->int_key)
  		{
	  	  return -1;
		}
#ifdef NOTYET
  	  memcpy(keys->int_key, sproto->int_key_length, keys->int_key_sz);
#endif
	}

  return 0;
}

int
gdoi_srtp_get_policy_from_sa (struct sa *sa, u_int8_t **ret_buf,
                           	   size_t *ret_buf_sz)
{
  u_int8_t *srtp_tek_buf = 0;
  u_int8_t *buf = 0;
  size_t srtp_tek_sz;
  u_int8_t *attr, *attr_start;
  struct proto *proto;
  struct srtp_proto *sproto;

  proto = TAILQ_FIRST (&sa->protos);
  sproto = proto->data;

  /*
   * Set the SSRC
   */
  srtp_tek_sz = 0;
  srtp_tek_buf = NULL;
  srtp_tek_buf = gdoi_grow_buf(srtp_tek_buf, &srtp_tek_sz, 
							   (u_int8_t *)&sproto->ssrc, 
							   sizeof(sproto->ssrc));
  if (!srtp_tek_buf)
    {
      goto bail_out;
	}

  /*
   * Get the SPI for this TEK.
   */
  srtp_tek_buf = gdoi_grow_buf(srtp_tek_buf, &srtp_tek_sz, 
							   proto->spi[0], SRTP_SPI_SIZE);
  if (!srtp_tek_buf)
    {
      goto bail_out;
  }

  /*
   * BEGIN ATTRIBUTE PROCESSING
   * Allocate a block for building attributes. It's sized large enough
   * so that we think it will avoid buffer overflows....
   */
  attr_start = attr = calloc(1, ATTR_SIZE); 
  if (!attr)
    {
  	  log_print ("gdoi_srtp_get_policy: "
              	 "calloc(%d) failed", ATTR_SIZE);
  	  goto bail_out;
	}

  /*
   * Get Destination Address
   */
  attr = attribute_set_var (attr, SRTP_ATTR_DESTINATION_ADDRESS,
  							(u_int8_t *) &sproto->addr.s_addr, 
							sizeof(&sproto->addr.s_addr));
  attr = attribute_set_var (attr, SRTP_ATTR_DESTINATION_RTP_PORT,
  							(u_int8_t *)&sproto->port, sizeof(&sproto->port));

  /*
   * Put the cipher into the payload as attributes
   */
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER, sproto->cipher_type);
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER_MODE,
  							  		sproto->cipher_mode);
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER_KEY_LENGTH,
  							  		sproto->cipher_key_length);
  /*
   * Add the attributes to the tek payload
   */
  srtp_tek_buf = gdoi_grow_buf(srtp_tek_buf, &srtp_tek_sz, attr_start, 
                               (attr - attr_start));
  free (attr_start);
  if (!srtp_tek_buf)
    {
      goto bail_out;
	}

  *ret_buf = srtp_tek_buf;
  *ret_buf_sz = srtp_tek_sz;

  return 0;

bail_out:
    if (buf)
      {
        free (buf);
      }
    gdoi_free_attr_payloads();
    return -1;
}
