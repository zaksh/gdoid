/* DO NOT EDIT-- this file is automatically generated.  */

#include "sysdep.h"

#include "constants.h"
#include "gdoi_srtp_num.h"



struct constant_map srtp_attr_cst[] = {
  { SRTP_ATTR_SSRC, "SSRC", 0 }, 
  { SRTP_ATTR_DESTINATION_ADDRESS, "DESTINATION_ADDRESS", 0 }, 
  { SRTP_ATTR_DESTINATION_RTP_PORT, "DESTINATION_RTP_PORT", 0 }, 
  { SRTP_ATTR_DESTINATION_RTCP_PORT, "DESTINATION_RTCP_PORT", 0 }, 
  { SRTP_ATTR_ROLLOVER_COUNTER, "ROLLOVER_COUNTER", 0 }, 
  { SRTP_ATTR_CIPHER, "CIPHER", 0 }, 
  { SRTP_ATTR_CIPHER_MODE, "CIPHER_MODE", 0 }, 
  { SRTP_ATTR_CIPHER_KEY_LENGTH, "CIPHER_KEY_LENGTH", 0 }, 
  { SRTP_ATTR_SALT_KEY_LENGTH, "SALT_KEY_LENGTH", 0 }, 
  { SRTP_ATTR_AUTHENTICATION_ALGORITHM, "AUTHENTICATION_ALGORITHM", 0 }, 
  { SRTP_ATTR_REPLAY_WINDOW_SIZE, "REPLAY_WINDOW_SIZE", 0 }, 
  { SRTP_ATTR_SRTCP, "SRTCP", 0 }, 
  { SRTP_ATTR_SPI, "SPI", 0 }, 
  { SRTP_ATTR_GROUPID_KEYID, "GROUPID_KEYID", 0 }, 
  { SRTP_ATTR_RETURN_PIPE, "RETURN_PIPE", 0 }, 
  { SRTP_ATTR_CIPHER_KEY, "CIPHER_KEY", 0 }, 
  { 0, 0 }
};


struct constant_map srtp_cipher_cst[] = {
  { SRTP_CIPHER_AES, "AES", 0 }, 
  { 0, 0 }
};


struct constant_map srtp_cipher_mode_cst[] = {
  { SRTP_CIPHER_MODE_COUNTER_MODE, "COUNTER_MODE", 0 }, 
  { SRTP_CIPHER_MODE_F8_MODE, "F8_MODE", 0 }, 
  { 0, 0 }
};


struct constant_map srtp_auth_alg_types_cst[] = {
  { SRTP_AUTH_ALG_TYPES_UMAC, "UMAC", 0 }, 
  { 0, 0 }
};

