/* $Id: gdoi_srtp_client.c,v 1.3 2002/07/26 22:58:10 bew Exp $ */
/* $Source: /nfs/cscbz/gdoi/gdoicvs/gdoi/src/gdoi_srtp_client.c,v $ */

/* 
 * The license applies to all software incorporated in the "Cisco GDOI reference
 * implementation" except for those portions incorporating third party software 
 * specifically identified as being licensed under separate license. 
 *  
 *  
 * The Cisco Systems Public Software License, Version 1.0 
 * Copyright (c) 2001 Cisco Systems, Inc. All rights reserved.
 * Subject to the following terms and conditions, Cisco Systems, Inc., 
 * hereby grants you a worldwide, royalty-free, nonexclusive, license, 
 * subject to third party intellectual property claims, to create 
 * derivative works of the Licensed Code and to reproduce, display, 
 * perform, sublicense, distribute such Licensed Code and derivative works. 
 * All rights not expressly granted herein are reserved. 
 * 1.      Redistributions of source code must retain the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer.
 * 2.      Redistributions in binary form must reproduce the above 
 * copyright notice, this list of conditions and the following 
 * disclaimer in the documentation and/or other materials 
 * provided with the distribution.
 * 3.      The names Cisco and "Cisco GDOI reference implementation" must not 
 * be used to endorse or promote products derived from this software without 
 * prior written permission. For written permission, please contact 
 * opensource@cisco.com.
 * 4.      Products derived from this software may not be called 
 * "Cisco" or "Cisco GDOI reference implementation", nor may "Cisco" or 
 * "Cisco GDOI reference implementation" appear in 
 * their name, without prior written permission of Cisco Systems, Inc.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESSED OR IMPLIED 
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED 
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
 * PURPOSE, TITLE AND NON-INFRINGEMENT ARE DISCLAIMED. IN NO EVENT 
 * SHALL CISCO SYSTEMS, INC. OR ITS CONTRIBUTORS BE LIABLE FOR ANY 
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL 
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF 
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR 
 * BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF 
 * THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF 
 * SUCH DAMAGE. THIS LIMITATION OF LIABILITY SHALL NOT APPLY TO 
 * LIABILITY FOR DEATH OR PERSONAL INJURY RESULTING FROM SUCH 
 * PARTY'S NEGLIGENCE TO THE EXTENT APPLICABLE LAW PROHIBITS SUCH 
 * LIMITATION. SOME JURISDICTIONS DO NOT ALLOW THE EXCLUSION OR 
 * LIMITATION OF INCIDENTAL OR CONSEQUENTIAL DAMAGES, SO THAT 
 * EXCLUSION AND LIMITATION MAY NOT APPLY TO YOU. FURTHER, YOU 
 * AGREE THAT IN NO EVENT WILL CISCO'S LIABILITY UNDER OR RELATED TO 
 * THIS AGREEMENT EXCEED AMOUNT FIVE THOUSAND DOLLARS (US) 
 * (US$5,000). 
 *  
 * ====================================================================
 * This software consists of voluntary contributions made by Cisco Systems, 
 * Inc. and many individuals on behalf of Cisco Systems, Inc. For more 
 * information on Cisco Systems, Inc., please see <http://www.cisco.com/>.
 *
 * This product includes software developed by Ericsson Radio Systems.
 */ 


#include <sys/types.h>
#include <sys/stat.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/uio.h>
#ifdef NOT_LINUX
#include <sys/sockio.h>
#endif
#include <arpa/inet.h>
#include <unistd.h>
#include <string.h>
#include <sys/un.h>

#include "log.h"
#include "util.h"
#include "string.h"
#include "transport.h"
#include "attribute.h"
#include "message.h"
#include "exchange.h"
#include "sa.h"
#include "gdoi_srtp.h"
#include "gdoi_srtp_client.h"
#include "gdoi_num.h"
#include "gdoi_srtp_num.h"

#define SRTP_CLIENT_PIPE "/tmp/srtp_to_gdoi"
#define SRTP_SIZE 65536

#define ATTR_SIZE (50 * ISAKMP_ATTR_VALUE_OFF)

struct srtp_transport {
  struct transport transport;
  int s;
};

struct srtp_group_info_type {
  struct cmd_header hdr;
  int group_id;
  char pipe_name[80];
} srtp_group_info;

struct srtp_group_policy_type {
  int group_id;
  char *attr;
} srtp_group_policy;

int s_from_srtp;

void srtp_remove (struct transport *);
static void srtp_report(struct transport *);
static int srtp_fd_set(struct transport *, fd_set *, int);
static int srtp_fd_isset(struct transport *, fd_set *);
static void srtp_handle_message(struct transport *);

static struct transport_vtbl srtp_transport_vtbl = {
  { 0 }, "srtp",
  NULL,
  srtp_remove,
  srtp_report,
  srtp_fd_set,
  srtp_fd_isset,
  srtp_handle_message,
  /* srtp_send_message */ NULL,
  /* srtp_get_dst */ NULL,
  /* srtp_get_src */ NULL
};

void
srtp_client_init (void)
{
  int s, ret;
  struct srtp_transport *t = 0;
  struct sockaddr_un pipe;
  mode_t old_umask;
  int on = 1;
 
  /*
   * Add the SRTP method to the transport list
   */
  transport_method_add (&srtp_transport_vtbl);

  /*
   * Create the IPC socket, and add it as a transport session.
   */
  t = malloc (sizeof *t);
  if (!t)
    {
      log_print ("srtp_client_init: malloc (%d) failed", sizeof *t);
      return;
    }

  t->transport.vtbl = &srtp_transport_vtbl;

  s = socket (AF_LOCAL, SOCK_STREAM, 0);
  if (s < 0)
    {
	  log_error ("srtp_client_init: socket failed");
	  return;
    }

  ret = setsockopt(s, SOL_SOCKET, SO_REUSEADDR, (void *)&on, sizeof(on));
  if (ret < 0)
    {
	  log_error ("srtp_client_init: bind failed");
	  return;
	}

  /*
   * Make sure it's not left over from another run.
   */
  unlink(SRTP_CLIENT_PIPE);

  /*
   * The mode of the pipe must be readable by all, so we need to adjust
   * our umask accordingly.
   */
  old_umask = umask(0044);
  
  bzero(&pipe, sizeof(struct sockaddr_un));
  pipe.sun_family = AF_LOCAL;
  strncpy(pipe.sun_path, SRTP_CLIENT_PIPE, sizeof(pipe.sun_path)-1);

  ret = bind(s, (struct sockaddr *) &pipe, SUN_LEN(&pipe));
  if (ret < 0)
    {
	  log_error ("srtp_client_init: bind failed");
	  return;
	}

  /*
   * Reset the process umask for security reasons.
   */
  (void) umask(old_umask);
  
  ret = listen(s, 1024);
  if (ret < 0)
    {
	  log_error ("listen failed");
	  return;
    }

  /*
   * Set the open socket in the transport structure.
   */
  t->s = s;

  transport_add (&t->transport);
  transport_reference (&t->transport);
  t->transport.flags |= TRANSPORT_LISTEN;
}

void
srtp_remove (struct transport *t)
{
  free (t);
}

static void
srtp_report (struct transport *t)
{
  log_debug (LOG_REPORT, 0, "srtp_report: Got Here!");
}

/*
 * Set transport T's socket in FDS, return a value useable by select(2)
 * as the number of file descriptors to check.
 */
static int
srtp_fd_set (struct transport *t, fd_set *fds, int bit)
{
  struct srtp_transport *u = (struct srtp_transport *)t;

  if (bit)
    FD_SET (u->s, fds);
  else
    FD_CLR (u->s, fds);

  return u->s + 1;
}

/* Check if transport T's socket is set in FDS.  */
static int
srtp_fd_isset (struct transport *t, fd_set *fds)
{
  struct srtp_transport *u = (struct srtp_transport *)t;

  return FD_ISSET (u->s, fds);
}

int srtp_msg_decode_attribute (u_int16_t type, u_int8_t *value, u_int16_t len,
							   void *arg)
{
  struct srtp_group_info_type *ptr = (struct srtp_group_info_type *) arg;

  switch (type)
    {
	case SRTP_ATTR_GROUPID_KEYID:
	  ptr->group_id = decode_32(value);
	  break;
    case SRTP_ATTR_RETURN_PIPE:
	  memcpy(ptr->pipe_name, value, len);
	  ptr->pipe_name[len] = 0; /* Terminate the string */
	  break;
	default:
      log_print ("srtp_msg_decode_attribute: Attribute not valid: %d", type);
	  return -1;
	}

return 0;
    
}

/*
 * For now, just stuff the info into a global struct. We can't yet
 * correlate an incoming msg with a finished GDOI session anyway, so 
 * have to restrict ourselves to one connection at a time.
 */
int
srtp_parse_msg (char *msg, int msg_len)
{
  struct cmd_header *hdr = (struct cmd_header *)msg;

  /*
   * Sanity check the header
   */
  if (hdr->version != 1)
    {
	  log_error("SRTP header unsupported version: %d\n", hdr->version);
	  return -1;
	}
  srtp_group_info.hdr.version = hdr->version;
  if (hdr->command != COMMAND_GET)
    {
	  log_error("SRTP header unsupported command: %d\n", hdr->command);
	  return -1;
	}
  srtp_group_info.hdr.command = hdr->command;
  srtp_group_info.hdr.sequence = hdr->sequence;
  srtp_group_info.hdr.pid = hdr->pid;

  attribute_map ((msg + sizeof(struct cmd_header)), 
  			 	 (msg_len - sizeof(struct cmd_header)),
  				 srtp_msg_decode_attribute, 
				 &srtp_group_info);

  return 0;
}

int
connect_to_client (char *out_fn)
{
  int s, ret;
  struct sockaddr_un pipe;

  s = socket (AF_LOCAL, SOCK_STREAM, 0);
  if (s < 0)
    {
	  log_error("socket open failed");
	  return -1;
    }
  
  bzero(&pipe, sizeof(struct sockaddr_un));
  pipe.sun_family = AF_LOCAL;
  strncpy(pipe.sun_path, out_fn, sizeof(pipe.sun_path)-1);

  ret = connect(s, (struct sockaddr *) &pipe, sizeof(pipe));
  if (ret < 0)
    {
	  log_error("connect failed");
	  return -1;
    }

  return s;
}

/* 
 * Clone a listen transport U, record a destination RADDR for outbound use.  
 */
static struct transport *
srtp_clone (struct srtp_transport *u)
{
  struct transport *t;
  struct srtp_transport *u2;

  t = malloc (sizeof *u);
  if (!t)
    {
      log_error ("srtp_clone: malloc (%d) failed", sizeof *u);
      return 0;
    }
  u2 = (struct srtp_transport *)t;

  memcpy (u2, u, sizeof *u);
  t->flags &= ~TRANSPORT_LISTEN;

  transport_add (t);

  return t;
}


/*
 * A message has arrived on transport T's socket.  If T is single-ended,
 * clone it into a double-ended transport which we will use from now on.
 * Package the message as we want it and continue processing in the message
 * module.
 */
static void
srtp_handle_message (struct transport *t)
{
  struct srtp_transport *u = (struct srtp_transport *)t;
  struct sockaddr_un from;
  int from_len = sizeof(from);
  struct message *msg;
  struct msghdr sock_msg;
  struct iovec iov[1];
  int c;
  char data_in[80];
  char name[80];
  int ret, count;

  /*
   * Accept happens after the select has woken.
   */
  c = accept(u->s, (struct sockaddr *) &from, &from_len);
  if (c < 0)
    {
	  log_error ("accept failed");
	  return;
    }

  sock_msg.msg_name = NULL;
  sock_msg.msg_namelen = 0;
  sock_msg.msg_control = 0;
  sock_msg.msg_controllen = 0;
  iov[0].iov_base = data_in;
  iov[0].iov_len = 80;
  sock_msg.msg_iov = iov;
  sock_msg.msg_iovlen = 1;

  count = recvmsg (c, &sock_msg, 0);
  if (count < 0)
    {
	  log_error("recvmsg failed");
	  return;
    }

  ret = srtp_parse_msg (data_in, count);
  if (ret < 0)
    {
	  return;
    }

  s_from_srtp = connect_to_client(&srtp_group_info.pipe_name[0]);
  if (s_from_srtp < 0)
    {
	  return;
    }

  /*
   * Keep a record of which fd's are waiting on info from which group.
   */

  /*
   * Make a specialized SRTP transport structure out of the incoming
   * transport.
   */
  t = srtp_clone (u);
  if (!t)
    {
	  log_error("srtp_clone failed");
      return;
	}

  msg = message_alloc (t, data_in, count);
  if (!msg)
    {
	  log_error("message_alloc failed");
      return;
	}
  /*
   * Kick off IKE based on the group-id passed in the message using msg.
   * HACK! Require a policy named "Group-XXXXX" where XXXXX is the number
   * of the group. This makes it easy to find the right phase 1 to kick off.
   */
  srtp_group_policy.group_id = srtp_group_info.group_id;
  /* 
   * HACK! Should clear only when know it's not been previously set. This
   *       should sort itself out when this is malloced.
   */
  srtp_group_policy.attr = NULL; 

  /* BEW: Need to parse the message to find the group id */ 
  sprintf(name, "Group-%d", srtp_group_info.group_id);
  log_debug (LOG_REPORT, 0, "srtp_handle_message: Starting exchange %s", name);
  exchange_establish(name, 0, 0);
}

char *attr_start, *attr;
char *buf;
struct cmd_header *hdr;

int
srtp_deliver_keys (struct sa *sa)
{
  struct proto *proto;
  struct srtp_proto *tek;
  int id;
  int buf_len;
  int ret;
  
  proto = TAILQ_FIRST (&sa->protos);
  if (!proto)
    {
      log_error ("srtp_deliver_keys: SRTP SA proto data missing");
      return -1;
    }
  if (proto->proto != SRTP_PROTO_SRTP)
    {
      log_error ("srtp_deliver_keys: SRTP SA expected, got %d", proto->proto);
      return -1;
	}

  tek = (struct srtp_proto *) proto->data;
  if (!tek)
    {
      log_error ("srtp_deliver_keys: SRTP SA TEK data missing");
      return -1;
    }

  /*
   * First translate the group policy name to group identity. This is to deal
   * with the HACK! in srtp_handle_message().
   */
  sscanf(sa->name, "Group-%d", &id);

  /*
   * Look up the id
   */
  if (id != srtp_group_policy.group_id)
	{ 
	  log_error ("srtp_deliver_keys: Group ID not found: %d", id);
	  return -1;
	}

  /*
   * If attr block hasn't been allocated do so now.
   */
  if (!srtp_group_policy.attr)
    {
      /*
       * Allocate a block for building attributes. It's sized large enough
	   * so that we think it will avoid buffer overflows....
       */
	  srtp_group_policy.attr = calloc(1, ATTR_SIZE);
	  if (!srtp_group_policy.attr)
	    {
	  	  log_error ("srtp_deliver_keys: malloc failed");
		  return -1;
		}
	}
  attr_start = attr = srtp_group_policy.attr;

  /*
   * Load the attr block up with tek values.
   */
  attr = attribute_set_var (attr, SRTP_ATTR_SPI, (u_int8_t *) proto->spi[0], 
  							proto->spi_sz[0]);
  attr = attribute_set_var (attr, SRTP_ATTR_SSRC, (u_int8_t *) &tek->ssrc, 
  							sizeof(tek->ssrc));
  attr = attribute_set_var (attr, SRTP_ATTR_DESTINATION_ADDRESS, 
 							(u_int8_t *) (u_int8_t *) &tek->addr.s_addr, 
							sizeof(tek->addr.s_addr));
  attr = attribute_set_var (attr, SRTP_ATTR_DESTINATION_RTP_PORT, 
  							(u_int8_t *) &tek->port, sizeof(tek->port));
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER, tek->cipher_type);
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER_MODE, tek->cipher_mode);
  /* 
   * BEW - Cipher length is redundant, because the length is also carried in the
   *       cipher attribute.
   */
  attr = attribute_set_basic (attr, SRTP_ATTR_CIPHER_KEY_LENGTH,
  							  tek->cipher_key_length);
  attr = attribute_set_var (attr, SRTP_ATTR_CIPHER_KEY, 
  							(u_int8_t *) tek->cipher_key, 
 						    tek->cipher_key_length);

  /*
   * Format the return message. Copy many of the fields from the originating
   * header to ensure they are the same.
   */
  buf_len = sizeof(struct cmd_header) + (attr - attr_start);
  buf = malloc(buf_len);

  hdr = (struct cmd_header *) buf;
  hdr->version = srtp_group_info.hdr.version;
  hdr->command = srtp_group_info.hdr.command;
  hdr->sequence = srtp_group_info.hdr.sequence;
  hdr->pid = srtp_group_info.hdr.pid;
  hdr->errno = 0;

  memcpy(buf + sizeof(struct cmd_header), attr_start, (attr - attr_start));
  /*
   * Don't free "attr_start" -- we're leaving the attributes around for 
   * later use.
   */

  /*
   * Send the message.
   */
  ret = send(s_from_srtp, buf, buf_len, 0);
  if (ret < 0)
    {
	  log_error ("srtp_deliver_keys: send failed");
	   return -1;
	}
  /*
   * HACK! If we send the TEKs too fast the client doesn't recieve them all.
   */
  sleep(1);

  return 0;
}
