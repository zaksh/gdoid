##################################################
		Configuration For KEY-SERVER
##################################################

If you want to have one authentication key for all members then use follwoing blocks

------------------------------------------

	[Phase 1]
	Default=            GDOI-group-members

	[GDOI-group-members]
	Phase=              1
	Transport=          udp
	Port=               848
	Configuration=      Default-main-mode
	Authentication=     multicastipsec

------------------------------------------

or if you want each member having different authentication key using following configuration

------------------------------------------

		[Phase 1]
		##IP of group member = Section name to look for
		192.168.1.1=      GDOI-group-member-host-a
		192.168.1.2=      GDOI-group-member-host-b
		192.168.1.3=      GDOI-group-member-host-c

		[GDOI-group-member-host-a] #section name 
		Phase=              1
		Transport=          udp
		Local-address=      192.168.1.4  ##Local address of Key-server
		Address=            192.168.1.1 ##Ip address of group memeber a
		Port=               848 ## Group memeber will be listening on this port
		Configuration=      Default-main-mode ##Default-main-mode section will be used for configuration
		Authentication=     multicastHostA  ##Authentication key for group member a

		[GDOI-group-member-host-b]
		Phase=              1
		Transport=          udp
		Local-address=      192.168.1.4
		Address=            192.168.1.2
		Port=               848
		Configuration=      Default-main-mode
		Authentication=     multicastHostB

		[GDOI-group-member-host-c]
		Phase=              1
		Transport=          udp
		Local-address=      192.168.1.4
		Address=            192.168.1.3
		Port=               848
		Configuration=      Default-main-mode
		Authentication=     multicastHostC

------------------------------------------

[Phase 2]
Passive-Connections=  IPsec-group-policy

[IPsec-group-policy]
Phase=              2
ISAKMP-peer=        GDOI-group-member
Configuration=      Default-group-mode
Group-ID=           Group-1

[Group-1]
ID-type=            KEY_ID
Key-value=          1234  ## Key for group to join

[Default-main-mode]
DOI=                GROUP
EXCHANGE_TYPE=      ID_PROT
Transforms=         3DES-SHA

[3DES-SHA]
ENCRYPTION_ALGORITHM=   3DES_CBC
HASH_ALGORITHM=         SHA
AUTHENTICATION_METHOD=  PRE_SHARED
GROUP_DESCRIPTION=      MODP_1024
Life=                   LIFE_600_SECS ##LIFE SHOULD BE SAME ON GROUP MEMBER AS ON KEY-SERVER

[LIFE_600_SECS]
LIFE_TYPE=        SECONDS
LIFE_DURATION=    600,450:720

[LIFE_3600_SECS]
LIFE_TYPE=        SECONDS
LIFE_DURATION=    3600,1800:7200

[LIFE_1000_KB]
LIFE_TYPE=        KILOBYTES
LIFE_DURATION=    1000,768:1536

[LIFE_32_MB]
LIFE_TYPE=        KILOBYTES
LIFE_DURATION=    32768,16384:65536

[LIFE_4.5_GB]
LIFE_TYPE=        KILOBYTES
LIFE_DURATION=    4608000,4096000:8192000

[QM-ESP-3DES-SHA-XF-MSEC]
TRANSFORM_ID=             3DES
ENCAPSULATION_MODE=       TRANSPORT
AUTHENTICATION_ALGORITHM= HMAC_SHA
Life=                     LIFE_600_SECS 

[Default-group-mode]
DOI=            GROUP
EXCHANGE_TYPE=  PULL_MODE
SA-KEK=         GROUP1-KEK
SA-TEKS=        GROUP1-TEK1, GROUP1-TEK2

[GROUP1-KEK]
Src-ID=               Group-kek-src
Dst-ID=               Group-kek-dst
SPI=                  abcdefgh01234567
ENCRYPTION_ALGORITHM= 3DES
SIG_HASH_ALGORITHM=   SHA
SIG_ALGORITHM=        RSA
DES_IV=               IVIVIVIV
DES_KEY1=             ABCDEFGH
DES_KEY2=             IJKLMNOP
DES_KEY3=             QRSTUVWX
RSA-Keypair=          /home/gdoid-1.2/samples/mytest/keys/rsakeys.der ## PATH TO RSA KEY
REKEY_PERIOD=         30

[Group-kek-src]
ID-type=              IPV4_ADDR
Address=              192.168.1.4 ## IP ADDRESS OF KEYING SERVER
Port=                 5001

[Group-kek-dst]
ID-type=              IPV4_ADDR
Address=              239.1.2.1
Port=                 5001

[GROUP1-TEK1]
Crypto-protocol=      PROTO_IPSEC_ESP
Src-ID=               Group-tek1-src ## MULTICAST GROUP SENDER
Dst-ID=               Group-tek1-dst ## MULTICAST GROUP DESTINATION
SPI=                  287484603
TEK_Suite=            QM-ESP-3DES-SHA-SUITE-MSEC
DES_KEY1=             ABCDEFGH  ## KEY FOR IPSEC ENCRYPTION
DES_KEY2=             IJKLMNOP ## KEY FOR IPSEC ENCRYPTION
DES_KEY3=             QRSTUVWX ## KEY FOR IPSEC ENCRYPTION
SHA_KEY=              12345678901234567890 ## KEY FOR IPSEC AUTHENTICATION

[Group-tek1-src]
ID-type=              IPV4_ADDR ## For other types you can look at src/ipsec_num.c:62
Address=              192.168.1.1  ## MULTICAST GROUP SENDER IP ADDRESS
Port=                 1024

[Group-tek1-dst]
ID-type=              IPV4_ADDR
Address=              239.1.1.1 ## MULTICAST ADDRESS
Port=                 1024

[GROUP1-TEK2]
Crypto-protocol=      PROTO_IPSEC_ESP
Src-ID=               Group-tek2-src
Dst-ID=               Group-tek1-dst
SPI=                  860146909
TEK_Suite=            QM-ESP-3DES-SHA-SUITE-MSEC
DES_KEY1=             ijklmnop
DES_KEY2=             qrstuvwx
DES_KEY3=             abcdefgh
SHA_KEY=              98765432109012345678

[Group-tek2-src]
ID-type=              IPV4_ADDR
Address=              192.168.1.2 ## MULTICAST SOURCE IP ADDRESS of GROUP1-TEK2
Port=                 1024

[QM-ESP-3DES-SHA-SUITE-MSEC]
Protocols=            QM-ESP-3DES-SHA-MSEC

[QM-ESP-3DES-SHA-MSEC]
PROTOCOL_ID=          IPSEC_ESP
Transforms=           QM-ESP-3DES-SHA-XF-MSEC
