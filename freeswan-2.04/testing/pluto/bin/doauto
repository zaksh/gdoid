#!/bin/sh
# start of an automatic testing script
#
# Note: the name of a test affects processing.
#  *-rsa|*-rsa-*: keys get loaded
#  *-pl|*-pl-*: extracts of Pluto Logs are added to the whack log
#
# RCSID $Id: doauto,v 1.30 2003/10/01 06:53:52 dhr Exp $

# sometimes CMP=diff is useful
CMP=${CMP:-cmp}

DOPLUTO=dopluto
DOWHACK=dowhack

DFAIL=""
XFAIL=""
SFAIL=""
REFMISSING=""
CFAIL=""

# slow tests: failure takes time.  Not automatically run.
# 	ipsec-wk-rsa
#	ipsec-wk-dnsrsa
#	clear-neg-nc-pl
#	clear-neg-fc-pl
# tests requiring ADNS delay impairments (slow):
#	ipsec-oppo-race
#	ipsec-oppo-race-rini-net
#	ipsec-oppo-race-iinr-net
# oddballs:
#	stipple-parallel stipple-serial

case "$#" in
0)	set - 	isakmp-psk isakmp-rsa isakmp-rsa-case isakmp-rsa-dot \
		ipsec-psk ipsec-rsa \
		ipsec-rsa-time-neg ipsec-rsa-time-trunc \
		ipsec-rsa-time-neg-dontrekey ipsec-rsa-time-trunc-dontrekey \
		ipsec-rsa-delete ipsec-rsa-c ipsec-rsa-co \
		ipsec-psk-rw ipsec-psk-id-rw ipsec-rsa-rw \
		isakmp-dnsrsa isakmp-dnsrsa-case isakmp-dnsrsa-dot \
		ipsec-dnsrsa ipsec-dnsrsa-delete ipsec-dnsrsa-c ipsec-dnsrsa-co \
		ipsec-dnsrsa-rw \
		ipsec-oppo ipsec-oppo-seq ipsec-oppo-twice \
		ipsec-oppo-narrow ipsec-oppo-miss \
		oe oe-noo clear block-pl reject-pl ipsec-oppo-group \
		isakmp-rsa-myid \
		regr-shunt-oppo regr-template-32-32 regr-oppo-narrow
	;;
esac

for t
do
    case "$t" in
    shutdown)
    	$DOWHACK shutdown
	;;
    --diff)
    	CMP="diff -w -u"
	;;
    *)
	echo $t:
	LD=log/$t
	[ -d "$LD" ] || mkdir -p "$LD"

	rm -f pluto/{east,west}/*.pid
	# make sure that we can core dump!
	ulimit -c unlimited

	rm -f pluto/{east,west}/*.pid
	# make sure that we can core dump!
	ulimit -c unlimited

	# start Initiator pluto (daemon forks to return control)
	$DOPLUTO west >$LD/pi-log 2>&1

	# start Responder pluto (daemon forks to return control)
	$DOPLUTO east >$LD/pr-log 2>&1

	(

	    $DOWHACK d$t || DFAIL="$DFAIL $t $?"

	    case "$t" in
	    *-rsa|*-rsa-*) $DOWHACK kall ;;
	    esac

	    $DOWHACK listen

	    $DOWHACK x$t || XFAIL="$XFAIL $t $?"

	    $DOWHACK shutdown || SFAIL="$SFAIL $t $?"

	    case "$t" in
	    *-pl|*-pl-*)
		echo
		echo ">>>Initiator:"
		sed 	-e '/^[|]/d' \
			-e '/Version/d' \
			-e '/Electric Fence/d' \
			$LD/pi-log
		echo
		echo ">>>Responder:"
		sed 	-e '/^[|]/d' \
			-e '/Version/d' \
			-e '/Electric Fence/d' \
			$LD/pr-log
		;;
	    esac

	    if [ -f pluto/west/core ];
	    then
                echo CORE west
		echo CORE west
		echo CORE west
            fi

	    if [ -f pluto/east/core ];
	    then
                echo CORE east
		echo CORE east
		echo CORE east
            fi

	) >$LD/wi-log


	if [ -f log.ref/$t/wi-log ]
	then
		sed -e '/^033 Can.t Opportunistically initiate for/s/RR of type TXT for \(.*\) was not found$/no host \1 for TXT record/' \
		    -e '/^\(002 .*received Vendor ID Payload; ASCII hash: \).*/s//\1XXXXXXXXXXXX/' \
		    -e 's/ [({]using isakmp#.*[})]//' \
		    -e 's/IPsec SA established {.*}/IPsec SA established/' \
		    -e '/^\=\=\= /d' \
		    -e 's/ {isakmp=#.*\/ipsec=#.*}//' \
			<$LD/wi-log >$LD/TMP
		mv $LD/TMP $LD/wi-log
		$CMP log.ref/$t/wi-log $LD/wi-log || CFAIL="$CFAIL $t $?"
	else
		REFMISSING="$REFMISSING $t"
	fi
	;;
    esac
done

[ -z "$DFAIL" ] || echo "definition failures:$DFAIL" >&2
[ -z "$XFAIL" ] || echo "execution failures:$XFAIL" >&2
[ -z "$SFAIL" ] || echo "shutdown failures:$SFAIL" >&2
[ -z "$REFMISSING" ] || echo "no reference logs:$REFMISSING" >&2
[ -z "$CFAIL" ] || echo "$CMP failures:$CFAIL" >&2
